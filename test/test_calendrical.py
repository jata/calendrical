"""
CALENDRICAL CALCULATIONS

Test cases for the module calendrical.

REFERENCEDATES has been checked against Sample Data in Calendrical Calculations.

Reference:
N. Dershowitz and E.M. Reingold, Calendrical Calculations, Cambridge, UK, 1997

"""
__author__	= "Jacob Tardell <jacob@tardell.se>"

import calendrical
import unittest
from test import test_support

#  R.D., Weekday, Gregorian month, day, year, ISO week, day, year, Hebrew month, day, year	
REFERENCEDATES = [
	[-214193,   0,    7, 24, -586,   29,  7, -586,    5, 10, 3174],
	[ -61387,   3,   12,  5, -168,   49,  3, -168,    9, 25, 3593],
	[  25469,   3,    9, 24,   70,   39,  3,   70,    7,  3, 3831],
	[  49217,   0,   10,  2,  135,   39,  7,  135,    7,  9, 3896],
	[ 171307,   3,    1,  8,  470,    2,  3,  470,   10, 18, 4230],
	[ 210155,   1,    5, 20,  576,   21,  1,  576,    3,  4, 4336],
	[ 253427,   6,   11, 10,  694,   45,  6,  694,    8, 13, 4455],
	[ 369740,   0,    4, 25, 1013,   16,  7, 1013,    2,  6, 4773],
	[ 400085,   0,    5, 24, 1096,   21,  7, 1096,    2, 23, 4856],
	[ 434355,   5,    3, 23, 1190,   12,  5, 1190,    1,  7, 4950],
	[ 452605,   6,    3, 10, 1240,   10,  6, 1240,   13,  8, 5000],
	[ 470160,   5,    4,  2, 1288,   14,  5, 1288,    1, 21, 5048],
	[ 473837,   0,    4, 27, 1298,   17,  7, 1298,    2,  7, 5058],
	[ 507850,   0,    6, 12, 1391,   23,  7, 1391,    4,  1, 5151],
	[ 524156,   3,    2,  3, 1436,    5,  3, 1436,   11,  7, 5196],
	[ 544676,   6,    4,  9, 1492,   14,  6, 1492,    1,  3, 5252],
	[ 567118,   6,    9, 19, 1553,   38,  6, 1553,    7,  1, 5314],
	[ 569477,   6,    3,  5, 1560,    9,  6, 1560,   12, 27, 5320],
	[ 601716,   3,    6, 10, 1648,   24,  3, 1648,    3, 20, 5408],
	[ 613424,   0,    6, 30, 1680,   26,  7, 1680,    4,  3, 5440],
	[ 626596,   5,    7, 24, 1716,   30,  5, 1716,    5,  5, 5476],
	[ 645554,   0,    6, 19, 1768,   24,  7, 1768,    4,  4, 5528],
	[ 664224,   1,    8,  2, 1819,   31,  1, 1819,    5, 11, 5579],
	[ 671401,   3,    3, 27, 1839,   13,  3, 1839,    1, 12, 5599],
	[ 694799,   0,    4, 19, 1903,   16,  7, 1903,    1, 22, 5663],
	[ 704424,   0,    8, 25, 1929,   34,  7, 1929,    5, 19, 5689],
	[ 708842,   1,    9, 29, 1941,   40,  1, 1941,    7,  8, 5702],
	[ 709409,   1,    4, 19, 1943,   16,  1, 1943,    1, 14, 5703],
	[ 709580,   4,   10,  7, 1943,   40,  4, 1943,    7,  8, 5704],
	[ 727274,   2,    3, 17, 1992,   12,  2, 1992,   13, 12, 5752],
	[ 728714,   0,    2, 25, 1996,    8,  7, 1996,   12,  5, 5756],
	[ 744313,   3,   11, 10, 2038,   45,  3, 2038,    8, 12, 5799],
	[ 764652,   0,    7, 18, 2094,   28,  7, 2094,    5,  5, 5854],
	]

class Test_Week(unittest.TestCase):
	def test_dayOfWeekFromFixed(self):
		"Test dayOfWeekFromFixed on REFERENCEDATES"
		for r in REFERENCEDATES:
			self.assertEqual(calendrical.dayOfWeekFromFixed(r[0]), r[1])

class Test_Gregorian(unittest.TestCase):
	def test_fixedFromGregorian(self):
		"Test fixedFromGregorian on REFERENCEDATES"
		for r in REFERENCEDATES:
			self.assertEqual(calendrical.fixedFromGregorian(r[4], r[2], r[3]), r[0])

	def test_gregorianYearFromFixed(self):
		"Test gregorianYearFromFixed on REFERENCEDATES"
		for r in REFERENCEDATES:
			self.assertEqual(calendrical.gregorianYearFromFixed(r[0]), r[4])

	def test_gregorianFromFixed(self):
		"Test gregorianFromFixed on REFERENCEDATES"
		for r in REFERENCEDATES:
			self.assertEqual(calendrical.gregorianFromFixed(r[0]), [r[4], r[2], r[3]])

class Test_ISO(unittest.TestCase):
	def test_fixedFromIso(self):
		"Test fixedFromIso on REFERENCEDATES"
		for r in REFERENCEDATES:
			self.assertEqual(calendrical.fixedFromIso(r[7], r[5], r[6]), r[0])

	def test_isoFromFixed(self):
		"Test isoFromFixed on REFERENCEDATES"
		for r in REFERENCEDATES:
			self.assertEqual(calendrical.isoFromFixed(r[0]), [r[7], r[5], r[6]])

class Test_Hebrew(unittest.TestCase):
	def test_fixedFromHebrew(self):
		"Test fixedFromHebrew on REFERENCEDATES"
		for r in REFERENCEDATES:
			self.assertEqual(calendrical.fixedFromHebrew(r[10], r[8], r[9]), r[0])

	def test_hebrewFromFixed(self):
		"Test hebrewFromFixed on REFERENCEDATES"
		for r in REFERENCEDATES:
			self.assertEqual(calendrical.hebrewFromFixed(r[0]), [r[10], r[8], r[9]])

# test support
def test_main():
	test_support.verbose = True
	test_support.run_unittest(Test_Week, Test_Gregorian, Test_ISO, Test_Hebrew)

if __name__ == '__main__':
	test_main()