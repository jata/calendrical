from distutils.core import setup

setup(	name = 'calendrical',
		version = '0.2',
		description = 'Calendrical calculations in Python',
		author = 'Jacob Tardell',
		author_email = 'jacob@tardell.se',
		license = 'MIT',
		url = 'http://jacob.tardell.se/',
		py_modules = ['calendrical'],
	)