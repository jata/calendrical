CALENDRICAL CALCULATIONS

This module provides a set of function for date conversion between
different calendars.

Currently the module supports
- Gregorian Calendar (the common calendar in the Western world),
- ISO Calendar (Gregorian week calendar used in some European 
	countries) and
- Hebrew calendar.

This code is an adaptation of the algorithms provided in the book
"Calendrical Calculations" by Reingold and Dershowitz. The functions 
in the module have similar names to the names used in the book.

Todo:
- more calendars
- class API
- more unit tests
- holiday functions
- optimization of calculations

Bugs:
None known at this moment.

Please, report bugs to jacob@tardell.se.

Author:
Jacob Tardell, Stockholm 2020, jacob@tardell.se

License:
Copyright (c) 2020 Jacob Tardell

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.